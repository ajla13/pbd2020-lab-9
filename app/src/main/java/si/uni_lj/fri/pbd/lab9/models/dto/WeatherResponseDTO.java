package si.uni_lj.fri.pbd.lab9.models.dto;

import com.google.gson.annotations.SerializedName;

public class WeatherResponseDTO {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("coord")
    private CoordDTO coord;
    @SerializedName("main")
    private MainDTO main;
    @SerializedName("dt")
    private int dt;

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public CoordDTO getCoord() {
        return coord;
    }

    public int getDt() {
        return dt;
    }

    public MainDTO getMain() {
        return main;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCoord(CoordDTO coord) {
        this.coord = coord;
    }

    public void setDt(int dt) {
        this.dt = dt;
    }

    public void setMain(MainDTO main) {
        this.main = main;
    }
}
