package si.uni_lj.fri.pbd.lab9.models.dto;

import com.google.gson.annotations.SerializedName;

public class CoordDTO {
    @SerializedName("Lon")
    private float lon;
    @SerializedName("Lat")
    private float lat;

    public float getLat() {
        return lat;
    }

    public float getLon() {
        return lon;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }
}
