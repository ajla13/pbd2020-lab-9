package si.uni_lj.fri.pbd.lab9.rest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import si.uni_lj.fri.pbd.lab9.models.dto.WatherResponsesDTO;

public interface RestAPI {

    @GET("data/2.5/box/city?")
    Call<WatherResponsesDTO.WeatherResponsesDTO> getCurrentWeatherData(@Query("bbox") String bbox,
                                                                       @Query("APPID") String app_id);

}
