package si.uni_lj.fri.pbd.lab9;

import androidx.fragment.app.FragmentActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import si.uni_lj.fri.pbd.lab9.models.dto.WatherResponsesDTO;
import si.uni_lj.fri.pbd.lab9.models.dto.WeatherResponseDTO;
import si.uni_lj.fri.pbd.lab9.rest.RestAPI;

public class MapsActivity extends FragmentActivity implements  GoogleMap.OnCameraIdleListener  {

    private GoogleMap mMap;
    private RestAPI mRestClient;
    private ConnectivityManager mNwManager;
    private ConnectivityManager.NetworkCallback
            mNwCallback;
    private boolean mOnUnmetered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        mRestClient=ServiceGenerator.createService(RestAPI.class);
        mOnUnmetered=false;
        mNwCallback= new ConnectivityManager.NetworkCallback();
        mOnUnmetered=true;
        mNwManager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        mNwManager.registerDefaultNetworkCallback(mNwCallback);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mNwManager.unregisterNetworkCallback(mNwCallback);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnCameraIdleListener(this);
        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(46, 14.5),
                8.0f) );

    }

    @Override
    public void onCameraIdle() {
        LatLngBounds bounds=mMap.getProjection().getVisibleRegion().latLngBounds;
        String bbox;
         double latBottom= bounds.southwest.latitude;
         double lonLeft=bounds.southwest.longitude;
         double latUp=bounds.northeast.latitude;
         double lonRight=bounds.northeast.longitude;
         String first=String.valueOf(lonLeft);
         String second=String.valueOf(latBottom);
         String third=String.valueOf(lonRight);
         String fourth=String.valueOf(latUp);
         bbox=first+","+second+","+third+","+fourth+","+"10";
                 mRestClient.getCurrentWeatherData(bbox, Constants.API_KEY)
                .enqueue(new Callback<WatherResponsesDTO.WeatherResponsesDTO>() {

                    @Override
                    public void onResponse(Call<WatherResponsesDTO.WeatherResponsesDTO> call, Response<WatherResponsesDTO.WeatherResponsesDTO> response) {
                        List<WeatherResponseDTO> weather=response.body().getWeatherResponses();
                        for(int i=0; i<weather.size();i++){
                            mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(weather.get(i).getCoord().getLat(),
                                            weather.get(i).getCoord().getLon()))
                                    .title(weather.get(i).getName() + " " + weather.get(i).getMain().getTemp())
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED
                                    )));

                        }
                    }

                    @Override
                    public void onFailure(Call<WatherResponsesDTO.WeatherResponsesDTO> call, Throwable t) {

                    }
                    });


    }
}
