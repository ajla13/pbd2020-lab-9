package si.uni_lj.fri.pbd.lab9.models.dto;

import com.google.gson.annotations.SerializedName;

public class MainDTO {
    @SerializedName("temp")
    private float temp;
    @SerializedName("temp_min")
    private float temp_min;
    @SerializedName("temp:max")
    private float temp_max;

    public float getTemp() {
        return temp;
    }

    public float getTemp_max() {
        return temp_max;
    }

    public float getTemp_min() {
        return temp_min;
    }

    public void setTemp(float temp) {
        this.temp = temp;
    }

    public void setTemp_max(float temp_max) {
        this.temp_max = temp_max;
    }

    public void setTemp_min(float temp_min) {
        this.temp_min = temp_min;
    }
}
