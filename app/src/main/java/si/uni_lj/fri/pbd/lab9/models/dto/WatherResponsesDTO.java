package si.uni_lj.fri.pbd.lab9.models.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WatherResponsesDTO {

    public class WeatherResponsesDTO {
        @SerializedName("cod")
        private String cod;
        @SerializedName("calctime")
        private float calctime;
        @SerializedName("cnt")
        private int cnt;
        @SerializedName("list")
        @Expose
        ArrayList<WeatherResponseDTO> list;
        // Getter Methods
        public String getCod() {
            return cod;
        }
        public float getCalctime() {
            return calctime;
        }
        public int getCnt() {
            return cnt;
        }
        // Setter Methods
        public void setCod( String cod ) {
            this.cod = cod;
        }
        public void setCalctime( float calctime ) {
            this.calctime = calctime;
        }
        public void setCnt(int cnt ) {
            this.cnt = cnt;
        }
        public ArrayList<WeatherResponseDTO> getWeatherResponses() {
            return list;
        }
    }

}
